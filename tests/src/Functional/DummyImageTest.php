<?php

namespace Drupal\Tests\dummyimage\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests dummyimage.
 *
 * @group dummyimage
 */
class DummyImageTest extends BrowserTestBase {

  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
  }

  protected static $modules = ['dummyimage', 'image'];

  protected $defaultTheme = 'starterkit_theme';

  /**
   * Test that dummyimage is altering urls according to the module settings.
   */
  public function testDummyImage() {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');

    // Create an image.
    $files = $this->drupalGetTestFiles('image');
    /** @var \StdClass $file */
    $file = reset($files);
    $original_uri = $file_system->copy($file->uri, 'public://', FileSystemInterface::EXISTS_RENAME);
    $width = rand(50, 400);
    $height = rand(80, 600);
    $altered_url = "http://dummyimage.com/{$width}x{$height}";

    $rendered = $this->renderImageFromUri($original_uri, $width, $height);
    $this->assertTrue(strpos($rendered, $altered_url) !== FALSE, "Setting is 'all' - image url was altered");

    // Kitteh!
    $config = \Drupal::configFactory()->getEditable('dummyimage.settings');
    $config->set('dummyimages_service', 'placekitten')->save();

    $altered_url = "http://placekitten.com/{$width}/{$height}";
    $rendered = $this->renderImageFromUri($original_uri, $width, $height);
    $this->assertTrue(strpos($rendered, $altered_url) !== FALSE, "Setting is 'all' and new provider is used - image url was altered correctly");

    $config = \Drupal::configFactory()->getEditable('dummyimage.settings');
    $config->set('dummyimages_generate', 'none')->save();
    $rendered = $this->renderImageFromUri($original_uri, $width, $height);
    $this->assertFalse(strpos($rendered, $altered_url), "Setting is 'none' - image url was not altered");

    $config = \Drupal::configFactory()->getEditable('dummyimage.settings');
    $config->set('dummyimages_generate', 'missing')->save();
    $rendered = $this->renderImageFromUri($original_uri, $width, $height);
    $this->assertFalse(strpos($rendered, $altered_url), "Setting is 'missing' - image url was not altered because image exists");

    // Delete the file we are rendering.
    $file_system->delete($original_uri);
    $rendered = $this->renderImageFromUri($original_uri, $width, $height);
    $this->assertTrue(strpos($rendered, $altered_url) !== FALSE, "Setting is 'missing' - image url was altered because image does not exist");
  }

  /**
   * Helper function to render an image.
   *
   * @param string $uri
   *   The image uri.
   * @param integer $width
   *   Image width.
   * @param integer $height
   *   Image height.
   *
   * @return string
   *   The rendered image.
   */
  protected function renderImageFromUri(string $uri, int $width, int $height) {
    $element = [
      '#theme' => 'image_style',
      '#style_name' => 'medium',
      '#uri' => $uri,
      '#width' => $width,
      '#height' => $height,
    ];
    return \Drupal::service('renderer')->renderPlain($element)->__toString();
  }
}
