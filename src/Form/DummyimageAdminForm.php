<?php

namespace Drupal\dummyimage\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DummyimageAdminForm extends ConfigFormBase {

  /**
   * The dummy image manager service.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $dummyImageManager;

  /**
   * Constructs a new DummyimageAdminForm object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $dummyImageManager
   *   The dummy image manager service.
   */
  public function __construct(PluginManagerInterface $dummyImageManager) {
    $this->dummyImageManager = $dummyImageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.dummyimage')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dummyimage.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dummyimage_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('dummyimage.settings');

    $config->set('dummyimages_generate', $form_state->getValue('dummyimages_generate'));
    $config->set('dummyimages_service', $form_state->getValue('dummyimages_service'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dummyimage.settings');

    $options = [
      'none' => $this->t('For no images'),
      'missing' => $this->t('For missing images'),
      'all' => $this->t('For all images'),
    ];

    $form['dummyimages_generate'] = [
      '#type' => 'radios',
      '#title' => $this->t('Use dummy images'),
      '#options' => $options,
      '#default_value' => $config->get('dummyimages_generate'),
    ];

    $manager = $this->dummyImageManager;
    $plugins = $manager->getDefinitions();
    $service_names = [];
    foreach ($plugins as $plugin) {
      $service_names[$plugin['id']] = $plugin['name'];
    }

    $form['dummyimages_service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#description' => $this->t('Select a image service to use'),
      '#default_value' => $config->get('dummyimages_service'),
      '#options' => $service_names,
    ];

    return parent::buildForm($form, $form_state);
  }
}
