<?php

namespace Drupal\dummyimage\Plugin\ImageProvider;

use Drupal\dummyimage\DummyImageProviderBase;

/**
 * @ImageProvider(
 *   id = "flickholdr",
 *   name = @Translation("Flickholdr"),
 *   url = "https://loremflickr.com/"
 * )
 */
class Flickholdr extends DummyImageProviderBase {

  public function getUrl($width, $height) {
    return 'https://loremflickr.com/' . $width . '/' . $height;
  }
}
