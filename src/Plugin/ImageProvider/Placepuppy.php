<?php

namespace Drupal\dummyimage\Plugin\ImageProvider;

use Drupal\dummyimage\DummyImageProviderBase;

/**
 * @ImageProvider(
 *   id = "placepuppy",
 *   name = @Translation("Placepuppy"),
 *   url = "https://placedog.net/"
 * )
 */
class Placepuppy extends DummyImageProviderBase {

  public function getUrl($width, $height) {
    return 'https://placedog.net/' . $width . '/' . $height;
  }
}
